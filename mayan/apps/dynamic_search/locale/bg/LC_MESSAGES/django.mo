��          �      �       H     I     R     b  	   q  "   {     �     �     �     �     �     �  !   �  �     �  �     $     5  !   U  &   w  Q   �  3   �     $     3     S  3   k  "   �  ]   �  #           	                                                 
        Advanced Advanced search Dynamic search Match all No search model matching the query No search results Search Search again Search for: %s Search results for: %s Search terms Try again using different terms.  When checked, only results that match all fields will be returned. When unchecked results that match at least one field will be returned. Project-Id-Version: Mayan EDMS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-10-17 07:05+0000
Last-Translator: Lyudmil Antonov <lantonov.here@gmail.com>
Language-Team: Bulgarian (http://www.transifex.com/rosarior/mayan-edms/language/bg/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: bg
Plural-Forms: nplurals=2; plural=(n != 1);
 Подробно Подробно търсене Динамично търсене Съвпадение на всички Няма модел за търсене, отговарящ на заявката Няма резултати от търсенето Търсене Потърсете отново Търсене за: %s Резултати от търсенето за: %s Термини на търсене Опитайте отново, като използвате различни термини. При отметка ще бъдат върнати само резултати, които съвпадат с всички полета. Когато не е отметнато ще бъдат показани резултати, които съвпадат поне с едно поле. 