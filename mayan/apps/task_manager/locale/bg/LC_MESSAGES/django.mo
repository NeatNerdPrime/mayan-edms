��          �      |      �     �     �  	        "     9     H     M     [     m     s     x     �     �     �  
   �     �  
   �     �      �  
        $  �  6     �  5   �       +      +   L     x     �  /   �     �     �     �  3   �     1  5   Q     �  *   �      �     �  Q   �  $   B  N   g                                                 
                            	                    Active tasks Active tasks in queue: %s Arguments Background task queues Default queue? Host Is transient? Keyword arguments Label Name Reserved tasks Reserved tasks in queue: %s Scheduled tasks Scheduled tasks in queue: %s Start time Task manager Test queue Type Unable to retrieve task list; %s View tasks Worker process ID Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-06-30 22:04+0000
Last-Translator: Lyudmil Antonov <lantonov.here@gmail.com>, 2019
Language-Team: Bulgarian (https://www.transifex.com/rosarior/teams/13584/bg/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: bg
Plural-Forms: nplurals=2; plural=(n != 1);
 Активни задачи Активни задачи на опашката: %s Аргументи Опашка на фонови задачи Опашка по подразбиране? Хост Преходен ли е? Аргументи на ключови думи Етикет Име Запазени задачи Запазени задачи на опашка: %s Планирани задачи Планирани задачи на опашка: %s Начален час Управление на задачите Опашка за тестове Тип Невъзможно извличане на списък със задачи; %s Преглед на задачите Идентификационен номер на работния процес 