# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Roberto Rosario, 2017
# Yaman Sanobar <yman.snober@gmail.com>, 2019
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-10-30 03:03-0400\n"
"PO-Revision-Date: 2017-06-30 22:04+0000\n"
"Last-Translator: Yaman Sanobar <yman.snober@gmail.com>, 2019\n"
"Language-Team: Arabic (https://www.transifex.com/rosarior/teams/13584/ar/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ar\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"

#: apps.py:24 links.py:12 permissions.py:7
msgid "Task manager"
msgstr ""

#: apps.py:32
msgid "Label"
msgstr "العنوان"

#: apps.py:35
msgid "Name"
msgstr "اسم"

#: apps.py:38
msgid "Default queue?"
msgstr ""

#: apps.py:42
msgid "Is transient?"
msgstr ""

#: apps.py:46
msgid "Type"
msgstr "النوع"

#: apps.py:49
msgid "Start time"
msgstr ""

#: apps.py:52
msgid "Host"
msgstr ""

#: apps.py:56
msgid "Arguments"
msgstr ""

#: apps.py:60
msgid "Keyword arguments"
msgstr ""

#: apps.py:64
msgid "Worker process ID"
msgstr ""

#: links.py:16 views.py:15
msgid "Background task queues"
msgstr ""

#: links.py:20
msgid "Active tasks"
msgstr ""

#: links.py:24
msgid "Reserved tasks"
msgstr ""

#: links.py:28
msgid "Scheduled tasks"
msgstr ""

#: permissions.py:10
msgid "View tasks"
msgstr ""

#: tests/literals.py:5
msgid "Test queue"
msgstr ""

#: views.py:30
#, python-format
msgid "Active tasks in queue: %s"
msgstr ""

#: views.py:42
#, python-format
msgid "Unable to retrieve task list; %s"
msgstr ""

#: views.py:56
#, python-format
msgid "Scheduled tasks in queue: %s"
msgstr ""

#: views.py:68
#, python-format
msgid "Reserved tasks in queue: %s"
msgstr ""
