��    	      d      �       �      �   ]   �      Q     V     c     i  D   n     �  �  �  *   <  �   g     %  2   :     m     �  �   �  $   ,                                       	       Creation datetime Default amount of time in seconds after which a resource lock will be automatically released. Lock Lock manager Locks Name Path to the class to use when to request and release resource locks. Timeout Project-Id-Version: Mayan EDMS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-10-17 08:07+0000
Last-Translator: Lyudmil Antonov <lantonov.here@gmail.com>
Language-Team: Bulgarian (http://www.transifex.com/rosarior/mayan-edms/language/bg/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: bg
Plural-Forms: nplurals=2; plural=(n != 1);
 Дата и час на създаване Период по подразбиране в секунди, след което заключването на ресурсите ще бъде автоматично освободено. Заключване Управление на заключването Заключвания Име Път към класа, който да се използва, при заявка и освобождаване заключването на ресурси. Изтичане на времето 