��    9      �  O   �      �  ]   �  �   G  �     4    �   B  �   *	  �  �	  ^  �  �   �     �     �  
   �     �  ;   �  	   
  
             >  <   E     �  	   �     �     �     �  
   �     �     �     �  	   �  F        L  
   O     Z     _     n     �     �     �     �  N   �  1        A  �   H     �     �     �  V   �     O     `     g     o     w  5   {     �     �     �  f  �  k   9    �    �  �  �  *  S  &  ~  +  �  �  �    �     �     �     �     �  v   �  	   a  
   k  <   v  
   �  ~   �     =      P      n      w      �      �   -   �      �   5   �      2!  �   B!     �!     �!     �!  -   �!  #   ""     F"     Y"  @   l"     �"  �   �"  j   j#     �#  �   �#     �$  5   �$  4   %  e   O%     �%     �%     �%     �%     �%  V   �%     V&  ,   ]&     �&                      3   )       0   1          !   8          "      *   #   4            5       '   6          /              7           
         	                     .      &          -         9         ,             (                     +       $   %                     2       
                    %(setting_project_title)s is based on %(project_title)s
                 
                %(project_title)s is a free and open-source software brought to you with <i class="fa fa-heart text-danger" style="transform: rotate(10deg);"></i> by Roberto Rosario and contributors.
             
                For questions check the <a class="new_window" href="https://docs.mayan-edms.com">Documentation %(icon_documentation)s</a> or the <a class="new_window" href="https://wiki.mayan-edms.com">Wiki %(icon_wiki)s</a>.
             
                If you found a bug or have a feature idea, visit the <a class="new_window" href="https://forum.mayan-edms.com">Forum %(icon_forum)s</a> or open a ticket in the <a class="new_window" href="https://gitlab.com/mayan-edms/mayan-edms">Source code repository %(icon_source_code)s</a>.
             
                If you use %(project_title)s please <a class="new_window" href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=3PXN336XFXQNN">consider making a donation %(icon_social_paypal)s</a>
             
                It takes great effort to make %(project_title)s as feature-rich as it is. We need all the help we can get!
             
                Spread the word. Talk to your friends and colleagues about how awesome %(project_title)s is!
                Follow us on <a class="new_window" href="https://twitter.com/mayanedms">Twitter %(icon_social_twitter)s</a>, <a class="new_window" href="https://www.facebook.com/MayanEDMS/">Facebook %(icon_social_facebook)s</a>, or <a class="new_window" href="https://www.instagram.com/mayan_edms/">Instagram %(icon_social_instagram)s</a>
             
                The complete list of changes is available via the <a class="new_window" href="https://docs.mayan-edms.com/releases/index.html">Release notes %(icon_documentation)s</a> or the short version <a class="new_window" href="https://gitlab.com/mayan-edms/mayan-edms/blob/master/HISTORY.rst">Changelog %(icon_documentation)s</a>.
             
                You can also <a class="new_window" href="https://www.paypal.com/paypalme2/RobertoRosario">donate directly to the creator and lead developer.  %(icon_social_paypal)s</a>
             About Actions Appearance Are you sure? Before you can fully use Mayan EDMS you need the following: Bootstrap Bootswatch Build number: %(build_number)s Cancel Check you network connection and try again in a few moments. Close Dashboard Fancybox FontAwesome Getting started Identifier Insufficient permissions JQuery Form JQuery Match Height Lato font Maximum number of characters that will be displayed as the view title. No No results None Page not found Released under the license: Save Select 2 Server communication error Server error Settings updated, restart your installation for changes to take proper effect. Sorry, but the requested page could not be found. Submit There's been an error. It's been reported to the site administrators via e-mail and should be fixed shortly. Thanks for your patience. Toastr Toggle Dropdown Toggle navigation Total (%(start)s - %(end)s out of %(total)s) (Page %(page_number)s of %(total_pages)s) Total: %(total)s URI.js Version Warning Yes You don't have enough permissions for this operation. jQuery jQuery Lazy Load required Project-Id-Version: Mayan EDMS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-10-30 07:03+0000
Last-Translator: Roberto Rosario
Language-Team: Bulgarian (http://www.transifex.com/rosarior/mayan-edms/language/bg/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: bg
Plural-Forms: nplurals=2; plural=(n != 1);
 
                    %(setting_project_title)s се основава на%(project_title)s
                 
                %(project_title)s е безплатен софтуер с отворен код предоставен Ви <i class="fa fa-heart text-danger" style="transform: rotate(10deg);"></i> от Роберто Росарио и сътрудници.
             
                За въпроси проверете <a class="new_window" href="https://docs.mayan-edms.com">Документация %(icon_documentation)s</a> или <a class="new_window" href="https://wiki.mayan-edms.com">Уики %(icon_wiki)s</a>.
             
                Ако намерите бъг или имате идея за прибавка, посетете <a class="new_window" href="https://forum.mayan-edms.com">Форум %(icon_forum)s</a> или направете заявка в <a class="new_window" href="https://gitlab.com/mayan-edms/mayan-edms">Хранилище на изходен код %(icon_source_code)s</a>.
             
                Ако използвате %(project_title)s молим <a class="new_window" href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=3PXN336XFXQNN"> помислете си дали да направите дарение %(icon_social_paypal)s</a>
             
                Необходими са големи усилия за да се направи %(project_title)s с толкова много функции както е сега. Необходима ни е цялата помощ, която можем да получим!
             
                Разкажете за проекта. Кажете на Вашите приятели и колеги за това какъв прекрасен е  %(project_title)s!
                Следете ни на <a class="new_window" href="https://twitter.com/mayanedms">Twitter %(icon_social_twitter)s</a>, <a class="new_window" href="https://www.facebook.com/MayanEDMS/">Facebook %(icon_social_facebook)s</a>, или <a class="new_window" href="https://www.instagram.com/mayan_edms/">Instagram %(icon_social_instagram)s</a>
             
                Пълният списък на промените е наличен чрез <a class="new_window" href="https://docs.mayan-edms.com/releases/index.html">Бележки към версията %(icon_documentation)s</a> или кратката версия <a class="new_window" href="https://gitlab.com/mayan-edms/mayan-edms/blob/master/HISTORY.rst">Дневник на промените%(icon_documentation)s</a>.
             
                Може също <a class="new_window" href="https://www.paypal.com/paypalme2/RobertoRosario">да направите дарение направо на създателя и главен разработчик.  %(icon_social_paypal)s</a>
             Относно Действия Изглед Сигурен ли сте? Преди да можете да ползвате напълно Mayan EDMS необходимо е следното: Bootstrap Bootswatch Номер на конструкцията: %(build_number)s Отказ Проверете връзката с мрежата и опитайте отново след няколко секунди. Затваряне Контролно табло Fancybox FontAwesome Начален етап Идентификатор Недостатъчни разрешения jQuery формуляр jQuery съответствие на височина Шрифт Lato Максимален брой на знаците, които ще се изобразят в заглавката на изгледа. Не Няма резултати Няма Страницата не е намерена Издаден под лиценз: Запазване Избиране 2 Грешка при свързването със сървъра Грешка в сървъра Настройките са актуализирани, стартирайте отново Вашата инсталация за да подействат. Съжалявам, но заявената страница не може да бъде намерена. Подаване Има грешка. Тя е съобщена на администраторите на сайта чрез електронно съобщение и ще бъде отстранена скоро. Благодарим за търпението. Toastr Превключване на плаващо меню Превключване на навигацията Общо (%(start)s - %(end)s от %(total)s) (Страница %(page_number)s от %(total_pages)s) Общо: %(total)s URI.js Версия Предупреждение Да Нямате достатъчни разрешения за тази операция. jQuery jQuery мързеливо зареждане задължително 