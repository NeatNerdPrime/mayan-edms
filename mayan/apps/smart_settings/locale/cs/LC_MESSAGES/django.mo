��          �      |      �     �     	               -     J     O  
   h     s  "   v     �     �     �     �     �  d   �     `     o     u     �     �  �  �     l     �     �     �  !   �     �  "   �                     <     N  +   h  
   �     �  b   �     !     4  %   <     b     v                                                                                    
         	    "%s" not a valid entry. Edit Edit setting: %s Edit settings Enter the new setting value. Name Namespace: %s, not found Namespaces No Overrided by environment variable? Setting count Setting namespaces Setting updated successfully. Settings Settings in namespace: %s Settings inherited from an environment variable take precedence and cannot be changed in this view.  Smart settings Value Value must be properly quoted. View settings Yes Project-Id-Version: Mayan EDMS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-10-18 10:54+0000
Last-Translator: Michal Švábík <snadno@lehce.cz>
Language-Team: Czech (http://www.transifex.com/rosarior/mayan-edms/language/cs/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: cs
Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n >= 2 && n <= 4 && n % 1 == 0) ? 1: (n % 1 != 0 ) ? 2 : 3;
 "%s" není platná položka. Upravit Upravit nastavení: %s Upravit nastavení Zadejte novou hodnotu nastavení. název Jmenný prostor: %s, nebyl nalezen Jmenné prostory Ne Přepsat proměnná prostředí? Počet nastavení Nastavení oborů názvů Nastavení bylo úspěšně aktualizováno. Nastavení Nastavení v oboru názvů: %s Nastavení zděděná z proměnné prostředí mají přednost a nelze je v tomto pohledu změnit. Cyhtré nastavení Hodnota Hodnota musí být správně uvedena. Zobrazit nastavení Ano 